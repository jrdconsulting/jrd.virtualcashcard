﻿using System.Diagnostics;
using NUnit.Framework;

namespace jrd.VirtualCashCard.Tests
{
    public static class NUnitExtensions
    {
        [DebuggerStepThrough]
        public static void ShouldEqual<T>(this T item, T other)
        {
            Assert.That(item, Is.EqualTo(other));
        }

        [DebuggerStepThrough]
        public static void ShouldBeNull<T>(this T item)
        {
            Assert.That(item, Is.Null);
        }

        [DebuggerStepThrough]
        public static void ShouldNotBeNull<T>(this T item)
        {
            Assert.That(item, Is.Not.Null);
        }

        [DebuggerStepThrough]
        public static void ShouldBeTrue(this bool item)
        {
            Assert.That(item, Is.True);
        }

        [DebuggerStepThrough]
        public static void ShouldBeFalse(this bool item)
        {
            Assert.That(item, Is.False);
        }
    }
}
