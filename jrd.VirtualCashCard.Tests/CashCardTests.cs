﻿using System;
using System.Linq;
using System.Threading.Tasks;
using jrd.VirtualCashCard.Core;
using NUnit.Framework;

namespace jrd.VirtualCashCard.Tests
{
    [TestFixture]
    public class CashCardTests
    {
        [Test]
        public void WhenCreatedShouldHaveCorrectBalance()
        {
            var pin = new Pin(1000);
            var account = new Account(56.55m);

            // act
            var cashCard = new CashCard(account, pin);

            var context = cashCard.EnterPin(pin);
            context.Balance.ShouldEqual(56.55m);
        }

        [Test]
        public void WhenEnteringAnIncorrectPin()
        {
            var originalPin = new Pin(2456);

            var account = new Account(56.55m);
            var cashCard = new CashCard(account, originalPin);

            var pin = new Pin(5610);
            var context = cashCard.EnterPin(pin);

            context.ShouldBeNull();
            // for our purposes we can infer bad pin from no context created
        }

        [Test]
        public void WhenEnteringAnCorrectPin()
        {
            var originalPin = new Pin(8539);

            var account = new Account(56.55m);
            var cashCard = new CashCard(account, originalPin);

            var pin = new Pin(8539);
            var context = cashCard.EnterPin(pin);

            context.ShouldNotBeNull();
        }

        [Test]
        public void WhenWithdrawingAndThereIsEnoughBalance()
        {
            var pin = new Pin(5421);
            var account = new Account(80.25m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // act
            var withdrawal = context.Withdraw(15m);

            withdrawal.Status.ShouldEqual(Status.Success);
            withdrawal.Amount.Value.ShouldEqual(15m);

            context.Balance.ShouldEqual(65.25m);
        }

        [Test]
        public void WhenWithdrawingAndThereIsNotEnoughBalance()
        {
            var pin = new Pin(5421);
            var account = new Account(10.88m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // act
            var withdrawal = context.Withdraw(11m);

            withdrawal.Status.ShouldEqual(Status.InsufficientBalance);
            withdrawal.Amount.Value.ShouldEqual(0);

            context.Balance.ShouldEqual(10.88m);
        }

        [Test]
        public void WhenDepositing()
        {
            var pin = new Pin(5421);
            var account = new Account(8.11m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // act
            var deposited = context.Deposit(1177.29m);

            deposited.ShouldBeTrue();

            context.Balance.ShouldEqual(1185.40m);
        }

        [Test]
        public void WhenDepositingNothing()
        {
            var pin = new Pin(5421);
            var account = new Account(8.11m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // act
            var deposited = context.Deposit(0);

            deposited.ShouldBeFalse();

            context.Balance.ShouldEqual(8.11m);
        }

        [Test]
        public void WhenDepositingNegativeAmounts()
        {
            var pin = new Pin(5421);
            var account = new Account(8.11m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // act
            var deposited = context.Deposit(-2);

            deposited.ShouldBeFalse();

            context.Balance.ShouldEqual(8.11m);
        }

        [Test]
        public void WhenManyTransactionsAreHappeningAtTheSameTime()
        {
            var withdraw = 3;
            var deposit = 10;
            
            var pin = new Pin(5421);
            var account = new Account(100m);
            var cashCard = new CashCard(account, pin);
            var context = cashCard.EnterPin(pin);

            // use a lot of iterations to ensure we get concurrency hits
            Parallel.ForEach(Enumerable.Range(1, 1000), i =>
            {
                Action action = i % 2 == 0 ? new Action(() => context.Withdraw(withdraw)) : () => context.Deposit(deposit);
                action();
            });

            var expectedBalance = 100 + (500 * deposit) - (500 * withdraw);
            context.Balance.ShouldEqual(expectedBalance);
        }

        [Test]
        public void WhenManyTransactionsAreHappeningAtTheSameTimeAcrossDifferentCardProcesses()
        {
            var withdraw = 3;
            var deposit = 10;

            var pin = new Pin(5421);
            var account = new Account(100m);
            var firstCashCard = new CashCard(account, pin);
            var secondCashCard = new CashCard(account, pin);

            var firstContext = firstCashCard.EnterPin(pin);
            var secondContext = secondCashCard.EnterPin(pin);

            // use a lot of iterations to ensure we get concurrency hits
            Parallel.ForEach(Enumerable.Range(1, 1000), i =>
            {
                Action action = i % 2 == 0 ? new Action(() => firstContext.Withdraw(withdraw)) : () => secondContext.Deposit(deposit);
                action();
            });

            var expectedBalance = 100 + (500 * deposit) - (500 * withdraw);
            firstContext.Balance.ShouldEqual(expectedBalance);
        }
    }
}
