﻿namespace jrd.VirtualCashCard.Core
{
    public class CardContext
    {
        readonly Account account;

        // important to be internal constructor - the tests/consumer can't easily create one
        internal CardContext(Account account)
        {
            this.account = account;
        }

        public Withdrawal Withdraw(Amount amount)
        {
            var debited = account.Debit(amount);

            return new Withdrawal(
                debited ? Status.Success : Status.InsufficientBalance,
                debited ? amount : 0);
        }

        public decimal Balance => account.CurrentBalance();

        public bool Deposit(Amount amount)
        {
            return account.Credit(amount);
        }
    }
}
