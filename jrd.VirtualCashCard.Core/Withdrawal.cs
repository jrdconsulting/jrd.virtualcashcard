﻿namespace jrd.VirtualCashCard.Core
{
    public class Withdrawal
    {
        public Withdrawal(Status status, Amount amount)
        {
            Status = status;
            Amount = amount;
        }

        public Status Status { get; }

        public Amount Amount { get; set; }
    }

    public enum Status
    {
        Unknown,
        Success,
        InsufficientBalance
    }
}