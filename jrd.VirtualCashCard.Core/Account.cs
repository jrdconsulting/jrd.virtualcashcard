﻿namespace jrd.VirtualCashCard.Core
{
    public class Account
    {
        Amount currentBalance;

        static readonly object accountPadlock = new object();

        public Account(Amount currentBalance)
        {
            this.currentBalance = currentBalance;
        }

        public decimal CurrentBalance()
        {
            lock (accountPadlock)
            {
                return currentBalance.Value;
            }
        }

        public bool Debit(Amount amount)
        {
            lock (accountPadlock)
            {
                var potentialBalance = currentBalance.Value - amount.Value;
                if (potentialBalance >= 0)
                {
                    currentBalance = new Amount(potentialBalance);
                    return true;
                }

                return false;
            }
        }

        public bool Credit(Amount amount)
        {
            if (amount.Value <= 0)
            {
                return false;
            }

            lock (accountPadlock)
            {
                currentBalance = new Amount(currentBalance.Value + amount.Value);
                return true;
            }
        }
    }
}