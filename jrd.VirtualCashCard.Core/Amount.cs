﻿namespace jrd.VirtualCashCard.Core
{
    public class Amount
    {
        public Amount(decimal amount)
        {
            Value = amount;
        }

        public decimal Value { get; }

        // This class is richer than a mere decimal and enables us to have currency etc in future
        // e.g. public CurrencyCode Currency { get; }

        // for now, without currency we can implicitly create from a decimal
        public static implicit operator Amount(decimal amount)
        {
            return new Amount(amount);
        }
    }
}