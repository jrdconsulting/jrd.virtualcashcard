﻿
namespace jrd.VirtualCashCard.Core
{
    public class CashCard
    {
        readonly Account account;
        readonly Pin originalPin;

        public CashCard(Account account, Pin pin)
        {
            this.account = account;
            this.originalPin = pin;
        }

        public CardContext EnterPin(Pin pin)
        {
            return pin.Number.Equals(originalPin.Number) ? new CardContext(account) : null;
        }
    }
}
