﻿namespace jrd.VirtualCashCard.Core
{
    public class Pin
    {
        public Pin(int pin)
        {
            Number = pin;
        }

        public int Number { get; }
    }
}
